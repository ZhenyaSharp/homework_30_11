﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task3
{
    class Program
    {
        static void Main(string[] args)
        {
            int a, b, sum = 0, sredAr, number;

            Console.WriteLine("Input a: ");
            a = int.Parse(Console.ReadLine());

            Console.WriteLine("Input b: ");
            b = int.Parse(Console.ReadLine());

            number = b - a + 1;

            while (a <= b)
            {
                sum += a;
                a++;
            }

            sredAr = sum / number;
            Console.WriteLine($" srednee arif-koe vseh chisel = {sredAr} ");
            Console.ReadKey();
        }
    }
}
