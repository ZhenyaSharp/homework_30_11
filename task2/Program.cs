﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2
{
    class Program
    {
        static void Main(string[] args)
        {
            int a, b, proiz=1;

            Console.WriteLine("Input a: ");
            a = int.Parse(Console.ReadLine());

            Console.WriteLine("Input b: ");
            b = int.Parse(Console.ReadLine());

            while (a <= b)
            {
                proiz *= a;               
                a++;
            }
            Console.WriteLine($" proizvedenie vseh chisel = {proiz} ");
            Console.ReadKey();
        }
    }
}
