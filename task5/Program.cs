﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task5
{
    class Program
    {
        static void Main(string[] args)
        {
            double n1 = 1, n2 = 100, sum = 0;
            while (n1 <= n2)
            {
                sum += 1 / n1;
                n1++;
            }
            Console.WriteLine($"proshel vsego {sum}");
            Console.ReadKey();
        }
    }
}
